;=================================
;File: code_9_18
;=================================

.code
	LDA var1         ;este bloco realiza: var1+var1+var1+var+var1.
	ADD var1
	ADD var1
	ADD var1
	ADD var1

end:
	INT exit

.data
	;syscall exit
	exit: DD 25
	var1: DD 10

.stack 1
