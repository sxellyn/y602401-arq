;=================================
;File: code_9_4
;=================================

.code
	LDA var1         ;este bloco realiza: var1 + var2.
	ADD var2

end:
	INT exit

.data
	;syscall exit
	exit: DD 25
	var1: DD -32768      ;int var1 = -32768.
	var2: DD 0xffff      ;int var2 = -1.

.stack 1
