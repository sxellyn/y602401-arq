;=================================
;File: code_9_8
;=================================

.code
	LDA var1         ;este bloco realiza: NAND(var1, var1).
	NAND var1        ;que equivale a: NOT(var1).

end:
	INT exit

.data
	;syscall exit
	exit: DD 25
	var1: DD 1111111111111111b      ;int var1 = 0xffff.

.stack 1

