;=================================
;File: code_9_2
;=================================

.code
	LDA var1         ;este bloco realiza: tmp = NAND(var1, var2).
	NAND var2        
	STA tmp

	NAND tmp         ;realiza: tmp = NAND(AC, tmp).

end:
	INT exit

.data
	;syscall exit
	exit: DD 25
	var1: DD 1111111111111111b      ;int var1 = 0xffff.
	var2: DD 1111111100000000b      ;int var1 = 0xff00.
	tmp: DD 0
.stack 1
