;=================================
;File: code_9_16
;=================================

.code
	LDA var1         ;este bloco realiza: SHIFT(var1, direita).
	SHIFT direita

end:
	INT exit

.data
	;syscall exit
	exit: DD 25
	var1: DD 1111111111111110b      ;int var1 = -2.
	direita: DD 0

.stack 1
