;=================================
;File: code_9_2
;=================================

.code
	LDA var1         ;este bloco realiza: var1 - var2.
	SUB var2

end:
	INT exit

.data
	;syscall exit
	exit: DD 25
	var1: DD 10      ;int var1 = 10.
	var2: DD 11      ;int var2 = 11.

.stack 1
